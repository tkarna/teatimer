var button_colors = {
    "#blackbutton": "#802000",
    "#greenbutton": "#6ad147",
    "#yellowbutton": "#f5df3d",
};

var button_times = {
    "#blackbutton": "2:00",
    "#greenbutton": "0:45",
    "#yellowbutton": "4:00",
};

var tea_colors = {
    "#blackbutton": "#ff5005",
    "#greenbutton": "#d8ff19",
    "#yellowbutton": "#ffd300",
};

function LightenDarkenColor(col,amt) {
    var usePound = false;
    if ( col[0] == "#" ) {
        col = col.slice(1);
        usePound = true;
    }

    var num = parseInt(col,16);

    var r = (num >> 16) + amt;

    if ( r > 255 ) r = 255;
    else if  (r < 0) r = 0;

    var b = ((num >> 8) & 0x00FF) + amt;

    if ( b > 255 ) b = 255;
    else if  (b < 0) b = 0;

    var g = (num & 0x0000FF) + amt;

    if ( g > 255 ) g = 255;
    else if  ( g < 0 ) g = 0;

    return (usePound?"#":"") + (g | (b << 8) | (r << 16)).toString(16);
};

function rgbColorToHex(color) {
    if (color.substr(0, 1) === '#') {
        return color;
    }
    var digits = /(.*?)rgb\((\d+), (\d+), (\d+)\)/.exec(color);

    var red = parseInt(digits[2]);
    var green = parseInt(digits[3]);
    var blue = parseInt(digits[4]);

    var rgb = blue | (green << 8) | (red << 16);
    return digits[1] + '#' + rgb.toString(16);
};

function raise_alert(){
    if ($('input[name=radio-alert]:checked').val() == 1) {
        alert("Tea is ready!");
    }
};

function timeStringToSeconds(timeString) {
    words = timeString.split(":")
    if (words.length == 1) {
        return Number(words[0])
    }
    return Number(words[0])*60. + Number(words[1]);
};

function secondsToTimeString(seconds) {
    var min = (seconds / 60) | 0;
    var sec = (seconds % 60) | 0;
    min = min < 10 ? "0" + min : min;
    sec = sec < 10 ? "0" + sec : sec;
    return min + ':' + sec;
}

function checkTimeString(timeString) {
    words = timeString.split(":")
    valid = /\S/.test(timeString) // not empty and not whitespace
    valid = valid && words.length == 1 || words.length == 2;
    for (i = 0; i < words.length; i++) {
        valid = valid && !isNaN(words[i]);
    }
    if (valid){
        valid = valid && timeStringToSeconds(timeString) > 0;
    }
    return valid;
}

window.onload = function () {
    var display = document.querySelector('#time');
    var seconds = 45;
    var timer = new CountDownTimer(seconds);
    var timeObj = CountDownTimer.parse(seconds);
    var teacolor = "#ffffff";
    $("#startbutton").button()
    timer.onstart = function() {
        $('#teacup_image').animate( { backgroundColor: teacolor }, timer.duration*1000 + 100, "swing", raise_alert);
        $("#accordion").accordion("option", "active", false);
        $("#timeinput").combobox("disable");
        $("#startbutton").button("disable");
        $.each(button_times, function(button_id, time) {
            $(button_id).disabled = true;
        });
    };
    timer.onfinish = function() {
        $("#timeinput").combobox("enable");
        $("#startbutton").button("enable");
        $.each(button_times, function(button_id, time) {
            $(button_id).disabled = false;
        });
    };

    // jquery ui init
    $("input[type='radio']").checkboxradio({
      icon: false
    });
    $(".controlgroup").controlgroup()
    $("#timeinput").combobox({
        select: function (event, ui){
            checkTimeString(ui.item.value);
            set_time(ui.item.value);
        },
        change: function (event, ui){
            set_time(ui.item.value);
        },
        click: function (event, ui){
            set_time(ui.item.value);
            start_timer();
        },
        valid: function (text) {
            return checkTimeString(text);
        },
        invalid_msg: '"{input}" is not valid time.\nEnter time in format 1:30 or in seconds 90.'
    });

    $("#accordion").accordion({
      collapsible: true,
      active: false
    });

    // TODO remove this ... writing to .value does not update combobox
    var chosen_time = document.getElementById("timeinput").value;

    update_time_disp();

    // set panel bg color from jquery ui theme, darken by -25%
    // var ui_bg_color = rgbColorToHex($("#startbutton").css("background-color"));
    // $('.innerpanel').css('background-color', LightenDarkenColor(ui_bg_color, -10));

    timer.onTick(format);

    document.querySelector('#startbutton').addEventListener('click', start_timer);
    document.querySelector('#teacup_image').addEventListener('click', start_timer);

    // add click callbacks to quick select buttons
    $.each(button_times, function(button_id, time) {
        $(button_id).button()
        $(button_id).click(
            function(){
                set_time(time);
                set_tea_color(tea_colors[button_id]);
            }
        );
    });

    // add custom hover effect to buttons: only lighten bg color a bit
    $.each(button_colors, function(button_id, color) {
        $(button_id).hover(
            function(){
                if (!timer.running) {
                    var new_color = LightenDarkenColor(color, 25);
                    $(this).css("background", new_color);
                }
            },
            function(){
                if (!timer.running) {
                    $(this).css("background", color);
                }
            }
        );
    });

    var rad = document.getElementsByName("radio-cuptype");
    for(var i = 0; i < rad.length; i++) {
        rad[i].onclick = function() {
            set_teacup_image(this.value);
        };
    }

    $('#teatable').find('tr').click( function(){
        var new_time = $(this).find('td:eq(3)').text();
        var new_tea_color = $(this).find('td:eq(4)').text();
        if (checkTimeString(new_time)){
            set_time(new_time);
            set_tea_color(new_tea_color);
        }
    });

    $('#teatable').find('tr').on('mouseover', function(){
        var is_header = $(this).find('th:eq(0)').text()
        if (!is_header){
            $(this).css("color", '#baec7e');
        }
    });
    $('#teatable').find('tr').on('mouseout', function(){
        var is_header = $(this).find('th:eq(0)').text()
        if (!is_header){
            $(this).css("color", '#ffffff');
        }
    });

    function set_time(new_time) {
        if (!timer.running) {
            chosen_time = new_time;
            update_time_disp();
        }
    };

    function set_tea_color(new_color) {
        if (!timer.running) {
            document.querySelector('#teacup_image').style.backgroundColor = new_color;
            teacolor = new_color;
        }
    };

    function set_teacup_image(teacuptype) {
        if (teacuptype == 'glass') {
            document.getElementById("teacup_image").src="images/teacup_glass_01_bright_transparent_small.png";
        } else {
            document.getElementById("teacup_image").src="images/teacup_porcelain_01_bright_transparent_small.png";
        }
    }

    function update_time_disp(){
        var timeString = chosen_time
        seconds = timeStringToSeconds(timeString);
        timer.duration = seconds;
        chosen_time = secondsToTimeString(seconds);
        $("#timeinput-input").val(chosen_time);
        timeObj = CountDownTimer.parse(seconds);
        format(timeObj.minutes, timeObj.seconds);
    }

    function start_timer(){
        if (!timer.running) {
            update_time_disp();
            timer.start();
        }
    }

    function format(minutes, seconds) {
        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;
        display.textContent = minutes + ':' + seconds;
    }

    // initially apply "black" button settings
    $("#blackbutton").trigger("click");
};
