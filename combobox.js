/*!
 * Custom combobox class
 * 
 * Modified from jquery combobox example
 * https://jqueryui.com/autocomplete/#combobox
 * 
 * This class accepts all text input for which options.valid returns true.
 * 
 * Changes:
 * - added options valid, invalid_msg
 * - changed select, change, click events
 *
 */

$.widget("custom.combobox", {
    _create: function() {
        this.wrapper = $("<span>")
            .addClass("custom-combobox")
            .insertAfter(this.element);

        this.element.hide();
        this._createAutocomplete();
        this._createShowAllButton();
        if (!("valid" in this.options) ||
            typeof this.options.valid == "undefined") {
            this.options.valid = function (text) { return true; };
        }
        if (!("invalid_msg" in this.options) ||
            typeof this.options.invalid_msg == "undefined") {
            this.options.invalid_msg = "'{input}' is not valid input";
        }
        this.cancelChange = false;
    },

    disable: function() {
        this.input.autocomplete('close');
        this.input.autocomplete('disable');
        this.input.attr('disabled', 'disabled');
    },

    enable: function() {
        this.input.autocomplete('enable');
        this.input.removeAttr('disabled');
    },

    _createAutocomplete: function() {
        var selected = this.element.children(":selected"),
            value = selected.val() ? selected.text() : "";

        var input_id = this.element.prop('id') + '-input'
        this.input = $("<input>")
            .appendTo(this.wrapper)
            .val(value)
            .attr("id", input_id)
            .attr("title", "")
            .addClass("custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left")
            .autocomplete({
                delay: 0,
                minLength: 0,
                source: $.proxy(this, "_source")
            })
            .tooltip({
                classes: {
                    "ui-tooltip": "ui-state-highlight"
                }
            });

        this._on(this.input, {
            autocompleteselect: function(event, ui) {
                ui.item.option.selected = true;
                this._trigger("select", event, {
                    item: ui.item.option
                });
                this.cancelChange = true; // prevent subsequent change event
            },

            autocompletechange: function(event, ui) {
                if ( this.cancelChange ) {
                    delete this.cancelChange;
                    return;
                }
                var text = this.input.val();
                if (this._check_valid_input(text)) {
                    this._trigger("change", event, {
                        item: {
                            label: text,
                            value: text,
                        }
                    });
                }
            },

            keyup:  function (event, ui) {
                if (event.keyCode == 13) {
                    var text = this.input.val();
                    if (this._check_valid_input(text)) {
                        this._trigger("click", event, {
                            item: {
                                label: text,
                                value: text,
                            }
                        });
                        this.cancelChange = true; // prevent subsequent change event
                    }
                }
            },
        });
    },

    _check_valid_input: function(text) {
        var valid = this.options.valid(text);
        if (!valid) {
            this._remove_bad_input(text);
        }
        return valid;
    },

    _remove_bad_input: function (value) {
        this.input
            .val("")
            .attr("title", this.options.invalid_msg.replace("{input}", value))
            .tooltip("open");
        this.element.val("");
        this._delay(function() {
            this.input.tooltip("close").attr("title", "");
        }, 4500);
        this.input.autocomplete("instance").term = "";
    },

    _createShowAllButton: function() {
        var input = this.input,
            wasOpen = false;

        $("<a>")
            .attr("tabIndex", -1)
            .attr("title", "Show All Items")
            .tooltip()
            .appendTo(this.wrapper)
            .button({
                icons: {
                    primary: "ui-icon-triangle-1-s"
                },
                text: false
            })
            .removeClass("ui-corner-all")
            .addClass("custom-combobox-toggle ui-corner-right")
            .on("mousedown", function() {
                wasOpen = input.autocomplete("widget").is(":visible");
            })
            .on("click", function() {
                input.trigger("focus");

                // Close if already visible
                if (wasOpen) {
                    return;
                }

                // Pass empty string as value to search for, displaying all results
                input.autocomplete("search", "");
            });
    },

    _source: function(request, response) {
        var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
        response(this.element.children("option").map(function() {
            var text = $(this).text();
            if (this.value && (!request.term || matcher.test(text)))
                return {
                    label: text,
                    value: text,
                    option: this
                };
        }));
    },

    _destroy: function() {
        this.wrapper.remove();
        this.element.show();
    }
});
