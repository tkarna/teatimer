#!/bin/bash
#
# Copies jquery theme files to the current project.
#
# NOTE silently overwrites all theme related files!

# set path to downloaded theme
# THEME=../jquery.themes/jquery-ui-1.12.1.le-frog
# THEME=../jquery.themes/jquery-ui-1.12.1.humanity
# THEME=../jquery.themes/jquery-ui-1.12.1.trontastic
# THEME=../jquery.themes/jquery-ui-1.12.1.mint-choc
# THEME=../jquery.themes/jquery-ui-1.12.1.dot-luv
THEME=../jquery.themes/jquery-ui-1.12.1.teatimer04

# set path to html root dir
DEST=.

cp $THEME/jquery-ui.css $DEST/
cp $THEME/jquery-ui.min.js $DEST/
rm $DEST/images/ui-*.png
cp $THEME/images/* $DEST/images/
